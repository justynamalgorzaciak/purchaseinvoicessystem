package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-02T00:14:40")
@StaticMetamodel(Invoice.class)
public class Invoice_ { 

    public static volatile SingularAttribute<Invoice, String> dateOfPayment;
    public static volatile SingularAttribute<Invoice, Date> dateOfModification;
    public static volatile SingularAttribute<Invoice, Double> netValue;
    public static volatile SingularAttribute<Invoice, Long> version;
    public static volatile SingularAttribute<Invoice, String> number;
    public static volatile SingularAttribute<Invoice, String> dateOfInvoice;
    public static volatile SingularAttribute<Invoice, String> reasonBeforeCancelled;
    public static volatile SingularAttribute<Invoice, Double> grossValue;
    public static volatile SingularAttribute<Invoice, String> comment;
    public static volatile SingularAttribute<Invoice, Double> valueOfTaxes;
    public static volatile SingularAttribute<Invoice, String> modifiedBy;
    public static volatile SingularAttribute<Invoice, Long> id;
    public static volatile SingularAttribute<Invoice, Long> vendorNumber;
    public static volatile ListAttribute<Invoice, Article> articles;
    public static volatile SingularAttribute<Invoice, Account> account;
    public static volatile SingularAttribute<Invoice, InvoiceStatus> status;

}