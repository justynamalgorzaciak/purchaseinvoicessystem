package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-02T00:14:40")
@StaticMetamodel(Employee.class)
public class Employee_ { 

    public static volatile SingularAttribute<Employee, String> firstName;
    public static volatile SingularAttribute<Employee, String> lastName;
    public static volatile SingularAttribute<Employee, String> telephone;
    public static volatile SingularAttribute<Employee, Long> id;
    public static volatile SingularAttribute<Employee, String> pesel;
    public static volatile SingularAttribute<Employee, Long> version;
    public static volatile SingularAttribute<Employee, String> email;
    public static volatile SingularAttribute<Employee, Account> account;

}