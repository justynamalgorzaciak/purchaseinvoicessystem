package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.AccessLevel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Employee;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-02T00:14:40")
@StaticMetamodel(Account.class)
public class Account_ { 

    public static volatile SingularAttribute<Account, String> password;
    public static volatile ListAttribute<Account, Invoice> invoices;
    public static volatile SingularAttribute<Account, Long> employeeId;
    public static volatile SingularAttribute<Account, Long> id;
    public static volatile SingularAttribute<Account, Employee> employee;
    public static volatile SingularAttribute<Account, String> login;
    public static volatile SingularAttribute<Account, AccessLevel> department;
    public static volatile SingularAttribute<Account, String> type;
    public static volatile SingularAttribute<Account, Long> version;

}