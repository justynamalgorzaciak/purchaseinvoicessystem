package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-02T00:14:40")
@StaticMetamodel(Article.class)
public class Article_ { 

    public static volatile SingularAttribute<Article, Double> taxRate;
    public static volatile SingularAttribute<Article, Double> taxForArticle;
    public static volatile SingularAttribute<Article, Double> price;
    public static volatile SingularAttribute<Article, Double> totalGrossPrice;
    public static volatile SingularAttribute<Article, String> invoiceNumber;
    public static volatile SingularAttribute<Article, String> nameOfArticle;
    public static volatile SingularAttribute<Article, Double> totalNetPrice;
    public static volatile SingularAttribute<Article, Long> id;
    public static volatile SingularAttribute<Article, Invoice> invoice;
    public static volatile SingularAttribute<Article, Integer> quantityOfArticles;

}