package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-02T00:14:40")
@StaticMetamodel(Vendor.class)
public class Vendor_ { 

    public static volatile SingularAttribute<Vendor, String> country;
    public static volatile SingularAttribute<Vendor, String> address;
    public static volatile ListAttribute<Vendor, Invoice> invoices;
    public static volatile SingularAttribute<Vendor, String> name;
    public static volatile SingularAttribute<Vendor, Long> id;
    public static volatile SingularAttribute<Vendor, Long> version;

}