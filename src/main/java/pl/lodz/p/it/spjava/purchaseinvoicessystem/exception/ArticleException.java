/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.exception;

import javax.persistence.PersistenceException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;

/**
 *
 * @author Justyna Małgorzaciak
 */
public class ArticleException extends BaseException{
    
        public final static String ARTICLE_EMPTY_FIELDS_MESSAGE = "Pole z nazwą artykułu nie może być puste!";

    public ArticleException(String message) {
        super(message);
    }

    private ArticleException(String message, Throwable cause) {
        super(message, cause);
    }

    private ArticleException(Throwable cause) {
        super(cause);
    }

    private ArticleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    private Article article;

    public Article getArticle() {
        return article;
    }

    public static ArticleException createForEmptyFields(Article article,
            PersistenceException pe) {
        ArticleException ae = new ArticleException(ARTICLE_EMPTY_FIELDS_MESSAGE, pe);
        ae.article = article;
        return ae;
    }
}
