/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.AccountDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.EmployeeDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.AccountFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.EmployeeFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.AccessLevel;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Employee;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;

/**
 *
 * @author Justyna Małgorzaciak
 */
@Stateful
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@Interceptors({pl.lodz.p.it.spjava.purchaseinvoicessystem.interceptor.LoggingInterceptor.class})
@RolesAllowed({"FINANCE_DEPARTMENT", "ADMINISTRATOR", "ADMINISTRATION_DEPARTMENT"})
public class AccountEndpoint extends AbstractEndpoint{

    @EJB
    private AccountFacade accountFacade;

    @EJB
    private EmployeeFacade employeeFacade;

    
    @RolesAllowed("ADMINISTRATOR")
    public void createAccount(AccountDTO accountDto, EmployeeDTO employeeDto) {

        Account account = new Account();

        account.setLogin(accountDto.getLogin());
        account.setPassword(accountDto.getPassword());
        
        switch(accountDto.getDepartment()){
            case ADMINISTRATOR:
            account.setDepartment(AccessLevel.ADMINISTRATOR);
            account.setType("ADMINISTRATOR");
            break;
            case DZIAŁ_ADMINISTRACJI:
            account.setDepartment(AccessLevel.DZIAŁ_ADMINISTRACJI);
            account.setType("ADMINISTRATION_DEPARTMENT");
            break;
            case DZIAŁ_KSIĘGOWY:
            account.setDepartment(AccessLevel.DZIAŁ_KSIĘGOWY);
            account.setType("FINANCE_DEPARTMENT");
            break;
        }

        Employee employee = new Employee();

        employee.setEmail(employeeDto.getEmail());
        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setPesel(employeeDto.getPesel());
        employee.setTelephone(employeeDto.getTelephone());

        employeeFacade.create(employee);

        account.setEmployee(employee);
        account.setEmployeeId(employee.getId());
        accountFacade.create(account);

        employee.setAccount(account);
        employeeFacade.edit(employee);
    }
    
   @RolesAllowed("ADMINISTRATOR")
    public List<Account> getAccounts() {
        return accountFacade.findAll();
    }

    public List<Employee> getEmployees() {
        return employeeFacade.findAll();
    }

    public void editAccount(Employee employee) {
        employeeFacade.edit(employee);
    }
    
    public void editMyAccount(AccountDTO accountDto, Account account) {
        account.setPassword(accountDto.getNewPassword());
        accountFacade.edit(account);
    }
    
    @RolesAllowed("ADMINISTRATOR")
    public Employee getAccountToEdition(Employee employee) {
        Employee entity = employeeFacade.find(employee.getId());
        return entity;
    }
    
    public Account getMyAccountToEdition(){
        List<Account> allAccounts = accountFacade.findAll();
        for (Account account : allAccounts) {
            if (account.getLogin().equals(ContextUtils.getUserName())) {
                return account;
    }
        }
                return allAccounts.get(0);
    }

    public void removeAccount(Employee employee) {
        employeeFacade.remove(employee);
    }
}
