package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.SessionContext;

abstract public class AbstractEndpoint {

    @Resource
    SessionContext sctx;
    protected static final Logger LOGGER = Logger.getGlobal();
    private String transactionId;

    public void afterBegin() {
        transactionId = Long.toString(System.currentTimeMillis())
                + ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
        LOGGER.log(Level.INFO, "Transakcja TXid={0} rozpoczęta w {1}, tożsamość: {2}",
                new Object[]{transactionId, this.getClass().getName(), sctx.getCallerPrincipal().getName()});
    }

    public void beforeCompletion() {
        LOGGER.log(Level.INFO, "Transakcja TXid={0} przed zatwierdzeniem w {1}, tożsamość {2}",
                new Object[]{transactionId, this.getClass().getName(), sctx.getCallerPrincipal().getName()});
    }

    public void afterCompletion(boolean committed) {
        LOGGER.log(Level.INFO, "Transakcja TXid={0} zakończona w {1} poprzez {3}, tożsamość {2}",
                new Object[]{transactionId, this.getClass().getName(), sctx.getCallerPrincipal().getName(), committed ? "ZATWIERDZENIE" : "ODWOŁANIE"});
    }

    protected Date parseDate(String stringDate) throws ParseException { //konwersja String do Date
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return format.parse(stringDate);
    }

    protected double roundNumber(double number) {  // zaokrąglenie kwot do dwóch miejsc po przecinku
        double newNumber = (double) Math.round(number * 100d) / 100d;
        return newNumber;
    }
}
