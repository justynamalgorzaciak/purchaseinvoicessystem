/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.account;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.AccountDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.EmployeeDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.AccountEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.exception.AccountException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "createAccountPageBean")
public class CreateAccountPageBean implements Serializable{
    
    @Inject
    AccountEndpoint accountEndpoint;
    
    private EmployeeDTO employeeDto;
    
    private AccountDTO accountDto;
    
    private static final Logger LOG = Logger.getLogger(CreateAccountPageBean.class.getName());

    public CreateAccountPageBean() {
      accountDto = new AccountDTO();
      employeeDto = new EmployeeDTO();
      
    }
   public String createAccount(){
        List<Account> listAccounts = accountEndpoint.getAccounts();
        try {
            for (Account listAccount : listAccounts) {
                if (listAccount.getLogin().equals(accountDto.getLogin())) {
                    throw new AccountException("Konto z takim loginem juz istnieje!");
                }
            }
            accountEndpoint.createAccount(accountDto, employeeDto);
            accountDto = new AccountDTO();

        } catch (AccountException ae) {
            LOG.log(Level.SEVERE, "Zgłoszenie w metodzie akcji wyjatku: ", ae);
            ContextUtils.emitInternationalizedMessage("createAccount:login", ae.getMessage());
            return null;
        }
        return "successAccount";
   }

    public EmployeeDTO getEmployeeDto() {
        return employeeDto;
    }

    public void setEmployeeDto(EmployeeDTO employeeDto) {
        this.employeeDto = employeeDto;
    }

    public AccountDTO getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDTO accountDto) {
        this.accountDto = accountDto;
    }
    
    public String returnToAccountsList(){
        return "accountsList";
    }
   
}
