/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Justyna Małgorzaciak
 */
@TableGenerator(name = "VendorIdGen", table = "GENERATOR", pkColumnName = "ENTITY_NAME", valueColumnName = "ID_RANGE", pkColumnValue = "Vendor", initialValue=100)
@Entity
public class Vendor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "VendorIdGen")
    @Column(name = "id")
    private Long id;
    
    @Version
    @Column(name = "version")
    private Long version;
    
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    @Column(name = "name")
    private String name;
    
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    @Column(name = "address")
    private String address;
    
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    @Column(name = "country")
    private String country;

    @OneToMany
    @JoinColumn(name = "vendor_id")
    
    private List<Invoice> invoices = new ArrayList();
    public List<Invoice> getInvoices() {
        return invoices;
    }
    
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendor)) {
            return false;
        }
        Vendor other = (Vendor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.purchasinginvoicessystem.model.Vendor[ id=" + id + " ]";
    }
    
}
