/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.account;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.AccountEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Employee;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "listAccountsPageBean")
public class ListAccountsPageBean implements Serializable {

    @Inject
    AccountEndpoint accountEndpoint;

    @Inject
    AccountController accountController;

    public List<Account> getAccounts() {
        return accountEndpoint.getAccounts();
    }

    public List<Employee> getEmployees() {
        return accountEndpoint.getEmployees();
    }

    public String editAccount(Employee employee) {
        accountController.getAccountToEdition(employee);
        return "editAccount";
    }

    public String removeAccount(Employee employee) {
        accountController.getAccountToEdition(employee);
        accountController.removeAccount();
        return "accountsList";
    }
}
