/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.invoice;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.InvoiceEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "listPageBean")
public class ListInvoicePageBean implements Serializable {

    @Inject
    private InvoiceEndpoint invoiceEndpoint;

    @Inject
    private InvoiceController invoiceController;

    @Inject
    private VendorController vendorController;

    DataModel<Invoice> invoicesDataModel;

    public List<Invoice> getInvoices() {
        return invoiceEndpoint.getInvoices();
    }

    public List<Invoice> getNewInvoices() {
        return invoiceEndpoint.getNew();
    }

    public List<Invoice> getInvoicesInProcess() {
        return invoiceEndpoint.getInProcess();
    }

    public List<Invoice> getAcceptedInvoices() {
        return invoiceEndpoint.getAccepted();

    }

    public List<Invoice> getCancelledInvoices() {
        return invoiceEndpoint.getCancelled();
    }

    public String editInvoice(Invoice invoice) {
        invoiceController.getInvoiceToEdition(invoice);
        vendorController.getVendorToEdition(invoice);
        return "editInvoice";
    }

    public String editCancelledInvoice(Invoice invoice) {
        invoiceController.getInvoiceToEdition(invoice);
        vendorController.getVendorToEdition(invoice);
        return "editInvoiceAfterCancelled";
    }

    public String cancelInvoice(Invoice invoice) throws ParseException {
        invoiceEndpoint.cancelInvoice(invoice);
        invoiceController.getInvoiceToEdition(invoice);
        return "beforeInvoiceCancelled";
    }

    public String sendInvoice(Invoice invoice) throws ParseException {
        invoiceEndpoint.sendInvoice(invoice);
        return "newInvoices";
    }

    public String acceptInvoice(Invoice invoice) throws ParseException {
        invoiceEndpoint.acceptInvoice(invoice);
        return "acceptedInvoices";
    }

    public String returnToMainPage() {
        return "main";
    }
}
