/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.exception;

import javax.persistence.PersistenceException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
public class AccountException extends BaseException{
    
        public final static String ACCOUNT_UNIQUE_NUMBER_MESSAGE = "Konto z takim loginem już istnieje!";

    public AccountException(String message) {
        super(message);
    }

    private AccountException(String message, Throwable cause) {
        super(message, cause);
    }

    private AccountException(Throwable cause) {
        super(cause);
    }

    private AccountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    private Account account;

    public Account getAccount() {
        return account;
    }

    public static AccountException createForUniqueInvoiceNumber(Account account,
            PersistenceException pe) {
        AccountException ae = new AccountException(ACCOUNT_UNIQUE_NUMBER_MESSAGE, pe);
        ae.account = account;
        return ae;
    }
}
