/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.InvoiceDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.VendorDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.AccountFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.ArticleFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.InvoiceFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.VendorFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Vendor;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;

/**
 *
 * @author Justyna Małgorzaciak
 */
@Stateful
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@Interceptors({pl.lodz.p.it.spjava.purchaseinvoicessystem.interceptor.LoggingInterceptor.class})
@RolesAllowed({"ADMINISTRATION_DEPARTMENT", "FINANCE_DEPARTMENT"})
public class InvoiceEndpoint extends AbstractEndpoint {

    @EJB
    private InvoiceFacade invoiceFacade;

    @EJB
    private ArticleEndpoint articleEndpoint;

    @EJB
    private ArticleFacade articleFacade;

    @EJB
    private VendorFacade vendorFacade;

    @EJB
    private AccountFacade accountFacade;
            
    private static final Logger LOG = Logger.getLogger(InvoiceEndpoint.class.getName());
    
    public void createNewInvoice(InvoiceDTO invoiceDto, VendorDTO vendorDto) throws ParseException {

        Invoice invoice = new Invoice();
        try {
            invoice.setNumber(invoiceDto.getNumber());
            invoice.setComment(invoiceDto.getComment());
            String dateOfInvoice = invoiceDto.getDateOfInvoice(); 
            String dateOfPayment = invoiceDto.getDateOfPayment();
            invoice.setStatus(InvoiceStatus.NOWA);
            setInvoiceVariables(invoice, dateOfPayment, dateOfInvoice);
            invoiceFacade.create(invoice);
        } catch (ParseException pe) {
            LOG.log(Level.SEVERE, pe.getMessage() );
        }
       Invoice invoiceEdit = getInvoiceByNumber(invoiceDto);
       Vendor vendor = createVendor(vendorDto, invoiceEdit);
       invoiceEdit.setVendorNumber(vendor.getId());
       invoiceFacade.edit(invoiceEdit);
    }

    public void cancelInvoice(Invoice invoice) throws ParseException {
        invoice.setStatus(InvoiceStatus.ODRZUCONA);
        setInvoiceChangingStatusVariable(invoice);
        invoiceFacade.edit(invoice);
    }

    public void sendInvoice(Invoice invoice) throws ParseException {
        invoice.setStatus(InvoiceStatus.W_PROCESIE);
        setInvoiceChangingStatusVariable(invoice);
        invoiceFacade.edit(invoice);
    }

    public void acceptInvoice(Invoice invoice) throws ParseException {
        invoice.setStatus(InvoiceStatus.ZAAKCEPTOWANA);
        setInvoiceChangingStatusVariable(invoice);
        invoiceFacade.edit(invoice);
    }

    public Invoice getInvoiceToEdition(Invoice invoice) {
        Invoice entity = invoiceFacade.find(invoice.getId());
        return entity;
    }

    public List<Invoice> getInvoices() {
        return invoiceFacade.findAll();
    }

    public void editInvoice(Invoice invoice) throws ParseException {
        
        Invoice invoiceEdit = invoiceFacade.find(invoice.getId());
        
        invoiceEdit.setComment(invoice.getComment());
        invoiceEdit.setReasonBeforeCancelled(invoice.getReasonBeforeCancelled());
        String dateOfInvoice = invoice.getDateOfInvoice();
        String dateOfPayment = invoice.getDateOfPayment();
        invoiceEdit.setStatus(invoice.getStatus());
        setInvoiceVariables(invoiceEdit, dateOfPayment, dateOfInvoice);
        
        invoiceFacade.edit(invoiceEdit);
    }

    public Invoice getInvoiceByNumber(InvoiceDTO invoiceDto) {
        List<Invoice> allInvoices = invoiceFacade.findAll();
        for (Invoice invoice : allInvoices) {
            if (invoice.getNumber().equals(invoiceDto.getNumber())) {
                return invoice;
            }
        }
        return allInvoices.get(0);
    }
    
    private void setInvoiceVariables(Invoice invoice, String dateOfPayment, String dateOfInvoice) throws ParseException {
        
        List<Article> articles = articleEndpoint.getArticles(invoice);
        List<Article> newArticles = new ArrayList<>();
        double totalNetAmount = 0;
        double totalGrossAmount = 0;
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
        
        invoice.setDateOfInvoice(dateOfInvoice);
        invoice.setDateOfPayment(dateOfPayment);
        invoice.setDateOfModification(parseDate(timeStamp));
        invoice.setModifiedBy(ContextUtils.getUserName());

        for (Article article : articles) { // Obliczanie całej kwoty faktury na podstawie kwot artykułów
            totalNetAmount += article.getTotalNetPrice();
            totalGrossAmount += article.getTotalGrossPrice();
            if(!invoice.getArticles().contains(article)){
                newArticles.add(article);
            }

        }
        invoice.setArticles(newArticles);
        invoice.setAccount(getAccountByLogin(invoice.getModifiedBy()));
        invoice.setNetValue(totalNetAmount);
        invoice.setGrossValue(totalGrossAmount);
        invoice.setValueOfTaxes(roundNumber((invoice.getGrossValue() - invoice.getNetValue())));

        for (Article article : newArticles) {

                article.setInvoice(invoice);
                articleFacade.edit(article);
        }
    }
    
    private void setInvoiceChangingStatusVariable(Invoice invoice) throws ParseException {
        
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
        invoice.setDateOfModification(parseDate(timeStamp));
        invoice.setModifiedBy(ContextUtils.getUserName());
        invoice.setAccount(getAccountByLogin(invoice.getModifiedBy()));
    }

    public Vendor createVendor(VendorDTO vendorDto, Invoice invoice) {

        List<Vendor> allVendors = vendorFacade.findAll();

        for (Vendor chosenVendor : allVendors) {
            if (chosenVendor.getName().equalsIgnoreCase(vendorDto.getName())) {
                setVendorVariables(chosenVendor, vendorDto, invoice);
                Vendor editVendor = editVendor(chosenVendor);
                return editVendor;
            }
        }

        Vendor vendor = new Vendor();
        setVendorVariables(vendor, vendorDto, invoice);
        vendorFacade.create(vendor);
        return vendor;

    }

    public Vendor editVendor(Vendor vendor) {
        vendorFacade.edit(vendor);
        return vendor;
    }

    public Vendor getVendorToEdition(Invoice invoice) {
        Vendor entity = vendorFacade.find(invoice.getVendorNumber());
        return entity;
    }

    private void setVendorVariables(Vendor vendor, VendorDTO vendorDto, Invoice invoice) {
        vendor.setName(vendorDto.getName());
        vendor.setAddress(vendorDto.getAddress());
        vendor.setCountry(vendorDto.getCountry());
        vendor.getInvoices().add(invoice);
    }

    public List<Invoice> getNew() {
        return invoiceFacade.findNewInvoices();
    }

    public List<Invoice> getInProcess() {
        return invoiceFacade.findInvoicesInProcess();
    }

    public List<Invoice> getCancelled() {
        return invoiceFacade.findCancelledInvoices();
    }

    public List<Invoice> getAccepted() {
        return invoiceFacade.findAcceptedInvoices();
    }

    public Account getAccountByLogin(String login) {
        List<Account> allAccounts = accountFacade.findAll();
        for (Account account : allAccounts) {
            if (account.getLogin().equals(login)) {
                return account;
            }
        }
        return allAccounts.get(0);
    }
}
