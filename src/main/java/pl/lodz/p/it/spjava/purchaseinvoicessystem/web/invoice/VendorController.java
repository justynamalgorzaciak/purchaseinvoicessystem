/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.invoice;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.InvoiceEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Vendor;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
public class VendorController implements Serializable{
    
    private Vendor editVendor;
            
    @EJB
    InvoiceEndpoint invoiceEndpoint;
    
     public void getVendorToEdition(Invoice invoice) {
      editVendor = invoiceEndpoint.getVendorToEdition(invoice);
    }
    
    public Vendor getEditedVendor(){
        return editVendor;
    }
    
    public void editVendor(){
        invoiceEndpoint.editVendor(editVendor);
    }
    
}
