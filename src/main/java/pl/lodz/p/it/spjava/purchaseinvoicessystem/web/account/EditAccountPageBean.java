/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.account;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.AccountDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.AccountEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Account;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Employee;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "editAccountPageBean")
public class EditAccountPageBean implements Serializable {

    @Inject
    AccountController accountController;

    @EJB
    AccountEndpoint accountEndpoint;

    private AccountDTO accountDto;

    public EditAccountPageBean() {
        accountDto = new AccountDTO();
    }

    public String editAccount() {
        accountController.editAccount();
        return "successEditAccount";
    }

    public Employee getEmployee() {
        return accountController.getEditedAccount();
    }

    public String editPassword() {
        Account account = accountEndpoint.getMyAccountToEdition();
        if (!account.getPassword().equals(accountDto.getPassword())) {
            ContextUtils.emitInternationalizedMessage("changePassword:oldPassword", "Hasło różni się od obecnego!");
            return null;
        } else if (!accountDto.getNewPassword().equals(accountDto.getConfirmedPassword())) {
            ContextUtils.emitInternationalizedMessage("changePassword:newPassword", "Potwierdzone hasło różni się od nowego hasła!");
            return null;
        } else if (accountDto.getPassword().equals(accountDto.getNewPassword())) {
            ContextUtils.emitInternationalizedMessage("changePassword:newPassword", "Hasło nie może być identyczne jak poprzednie!");
            return null;}
            else {
        accountEndpoint.editMyAccount(accountDto, account);
        return "changePasswordSuccess";}
    }

    public AccountDTO getAccountDto() {
        return accountDto;
    }

    public void setAccountDto(AccountDTO accountDto) {
        this.accountDto = accountDto;
    }

}
