/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Justyna Małgorzaciak
 */
@TableGenerator(name = "InvoiceIdGen", table = "GENERATOR", pkColumnName = "ENTITY_NAME", valueColumnName = "ID_RANGE", pkColumnValue = "Invoice", initialValue=100)
@Entity
@NamedQueries ({
    @NamedQuery(name="Invoice.getNew", query="SELECT i FROM Invoice i WHERE i.status = pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus.NOWA"),
    @NamedQuery(name="Invoice.getInProcess", query="SELECT i FROM Invoice i WHERE i.status = pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus.W_PROCESIE"),
    @NamedQuery(name="Invoice.getAccepted", query="SELECT i FROM Invoice i WHERE i.status = pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus.ZAAKCEPTOWANA"),
    @NamedQuery(name="Invoice.getCancelled", query="SELECT i FROM Invoice i WHERE i.status = pl.lodz.p.it.spjava.purchaseinvoicessystem.model.InvoiceStatus.ODRZUCONA")
})
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator="InvoiceIdGen")
    @Column(name = "id")
    private Long id;
    
    @Version
    @Column(name = "version")
    private Long version;
    
    @Column(name = "status")
    private InvoiceStatus status;
    
    @NotNull
    @Column(name = "number")
    private String number;
    
    @Column(name = "vendorNumber")
    private Long vendorNumber;
    
    @NotNull
    @Pattern(regexp = "(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}")
    @Column(name = "dateofinvoice")
    private String dateOfInvoice;
    
    
    @NotNull
    @Pattern(regexp = "(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}")
    @Column(name = "dateofpayment")
    private String dateOfPayment;

    @Column(name = "comment")
    private String comment;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "netvalue")
    private double netValue;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "grossvalue")
    private double grossValue;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "valueoftaxes")
    private double valueOfTaxes;
    
    @OneToMany(mappedBy = "invoice", cascade={CascadeType.REMOVE}, fetch= FetchType.EAGER)
    private List<Article> articles = new ArrayList();
    
    @ManyToOne
    private Account account;
    
    @NotNull
    @Column(name = "dateofmodification")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfModification;
    
    @NotNull
    @Column(name = "modifiedBy")
    private String modifiedBy;
    
    @Column(name="reasonBeforeCancelled")
    private String reasonBeforeCancelled;

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
    
    public InvoiceStatus getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getVendorNumber() {
        return vendorNumber;
    }

    public void setVendorNumber(Long vendor_number) {
        this.vendorNumber = vendor_number;
    }


    public String getDateOfInvoice() {
        return dateOfInvoice;
    }

    public void setDateOfInvoice(String dateOfInvoice) {
        this.dateOfInvoice = dateOfInvoice;
    }

    public String getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(String dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getReasonBeforeCancelled() {
        return reasonBeforeCancelled;
    }

    public void setReasonBeforeCancelled(String reasonBeforeCancelled) {
        this.reasonBeforeCancelled = reasonBeforeCancelled;
    }

    public double getNetValue() {
        return netValue;
    }

    public void setNetValue(double netValue) {
        this.netValue = netValue;
    }

    public double getGrossValue() {
        return grossValue;
    }

    public void setGrossValue(double grossValue) {
        this.grossValue = grossValue;
    }

    public double getValueOfTaxes() {
        return valueOfTaxes;
    }

    public void setValueOfTaxes(double valueOfTaxes) {
        this.valueOfTaxes = valueOfTaxes;
    }
            
    public Long getId() {
        return id;
    }

    public Date getDateOfModification() {
        return dateOfModification;
    }

    public void setDateOfModification(Date dateOfModification) {
        this.dateOfModification = dateOfModification;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoice)) {
            return false;
        }
        Invoice other = (Invoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.purchasinginvoicessystem.model.Invoice[ id=" + id + " ]";
    }
    
}
