/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.invoice;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.ArticleDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.ArticleEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Vendor;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "editPageBean")
public class EditInvoicePageBean implements Serializable {

    @Inject
    private ArticleEndpoint articleEndpoint;

    @Inject
    private InvoiceController invoiceController;

    @Inject
    private VendorController vendorController;
    
    private ArticleDTO articleDto;

    public EditInvoicePageBean() {
        articleDto = new ArticleDTO();
    }

    public Invoice getInvoice() {
        return invoiceController.getEditedInvoice();
    }

    public String editInvoice() throws ParseException {
        
        invoiceController.editInvoice();
        vendorController.editVendor();
        articleDto = new ArticleDTO();
        return "successEditInvoice";
    }
    
    public String cancelInvoice() throws ParseException{
        invoiceController.editInvoice();
        return "successCancel";
    }
    
    public Vendor getVendor(){
        return vendorController.getEditedVendor();
    }

    public String addArticle() {

        articleEndpoint.addArticle(articleDto, invoiceController.getEditedInvoice());
        articleDto = new ArticleDTO();
        return "editInvoice";
    }

    public ArticleDTO getArticleDto() {
        return articleDto;
    }

    public void setArticleDto(ArticleDTO articleDto) {
        this.articleDto = articleDto;
    }

    public List<Article> getArticles() {
        return articleEndpoint.getArticles(invoiceController.getEditedInvoice());
    }
}
