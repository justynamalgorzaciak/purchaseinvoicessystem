/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.dto;

import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.AccessLevel;

/**
 *
 * @author Justyna Małgorzaciak
 */
public class AccountDTO {
        
    private String login;
    
    private String password;
    
    private String newPassword;
    
    private String confirmedPassword;
    
    private AccessLevel department;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccessLevel getDepartment() {
        return department;
    }

    public void setDepartment(AccessLevel department) {
        this.department = department;
    }
    
    public AccessLevel[] getAccessLevels() {
    return AccessLevel.values();
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
