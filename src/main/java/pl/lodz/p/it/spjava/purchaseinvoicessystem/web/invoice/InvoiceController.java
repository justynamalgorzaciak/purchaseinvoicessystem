/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.invoice;

import java.io.Serializable;
import java.text.ParseException;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.InvoiceEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.exception.InvoiceException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
public class InvoiceController implements Serializable {
    
    private Invoice editedInvoice;
    
    @EJB
    InvoiceEndpoint invoiceEndpoint;
    
     public void getInvoiceToEdition(Invoice invoice) {
      editedInvoice = invoiceEndpoint.getInvoiceToEdition(invoice);
    }
    
    public Invoice getEditedInvoice(){
        return editedInvoice;
    }
    
    public void editInvoice() throws ParseException{
        invoiceEndpoint.editInvoice(editedInvoice);
    }
    
}
