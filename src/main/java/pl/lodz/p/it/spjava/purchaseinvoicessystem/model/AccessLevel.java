/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

/**
 *
 * @author Justyna Małgorzaciak
 */
public enum AccessLevel { //Poziomy dostępu a jednocześnie działy firmy wykorzystane do wyboru przy tworzeniu konta
    DZIAŁ_ADMINISTRACJI,
    DZIAŁ_KSIĘGOWY,
    ADMINISTRATOR;
}
