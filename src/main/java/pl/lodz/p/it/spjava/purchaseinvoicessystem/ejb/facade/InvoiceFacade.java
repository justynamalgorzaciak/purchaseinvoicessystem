/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
@Stateless
public class InvoiceFacade extends AbstractFacade<Invoice> {

    @PersistenceContext(unitName = "PISystem_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InvoiceFacade() {
        super(Invoice.class);
    }
    
    public List<Invoice> findNewInvoices(){
        TypedQuery<Invoice> tq = em.createNamedQuery("Invoice.getNew", Invoice.class);
        return tq.getResultList();
    }    
        
    public List<Invoice> findInvoicesInProcess(){
        TypedQuery<Invoice> tq = em.createNamedQuery("Invoice.getInProcess", Invoice.class);
        return tq.getResultList();
    }    
        
    public List<Invoice> findCancelledInvoices(){
        TypedQuery<Invoice> tq = em.createNamedQuery("Invoice.getCancelled", Invoice.class);
        return tq.getResultList();
    }    
    
        public List<Invoice> findAcceptedInvoices(){
        TypedQuery<Invoice> tq = em.createNamedQuery("Invoice.getAccepted", Invoice.class);
        return tq.getResultList();
    }  
}
