/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Vendor;

/**
 *
 * @author Justyna Małgorzaciak
 */
@Stateless
public class VendorFacade extends AbstractFacade<Vendor> {

    @PersistenceContext(unitName = "PISystem_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VendorFacade() {
        super(Vendor.class);
    }
    
}
