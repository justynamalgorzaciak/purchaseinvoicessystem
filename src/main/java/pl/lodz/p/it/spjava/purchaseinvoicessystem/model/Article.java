/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Justyna Małgorzaciak
 */
@TableGenerator(name = "ArticleIdGen", table = "GENERATOR", pkColumnName = "ENTITY_NAME", valueColumnName = "ID_RANGE", pkColumnValue = "Article", initialValue=100)
@Entity
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator="ArticleIdGen")
    @Column(name = "id")
    private Long id;
    

    
    @NotNull
    @Column(name = "name")
    private String nameOfArticle;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "price")
    private double price;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "quantity")
    private int quantityOfArticles;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "totalnetprice")
    private double totalNetPrice;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "totalgrossprice")
    private double totalGrossPrice;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "taxpayment")
    private double taxForArticle;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "taxrate")
    private double taxRate;
    
    @Column(name = "invoiceNumber")
    private String invoiceNumber;
    
    @ManyToOne
    @JoinColumn
    private Invoice invoice;

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
    
    public Long getId() {
        return id;
    }

    public int getQuantityOfArticles() {
        return quantityOfArticles;
    }

    public void setQuantityOfArticles(int quantityOfArticles) {
        this.quantityOfArticles = quantityOfArticles;
    }
    
    public String getNameOfArticle() {
        return nameOfArticle;
    }

    public void setNameOfArticle(String nameOfArticle) {
        this.nameOfArticle = nameOfArticle;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalNetPrice() {
        return totalNetPrice;
    }

    public void setTotalNetPrice(double totalNetPrice) {
        this.totalNetPrice = totalNetPrice;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getTotalGrossPrice() {
        return totalGrossPrice;
    }

    public void setTotalGrossPrice(double totalGrossPrice) {
        this.totalGrossPrice = totalGrossPrice;
    }

    public double getTaxForArticle() {
        return taxForArticle;
    }

    public void setTaxForArticle(double taxForArticle) {
        this.taxForArticle = taxForArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Article)) {
            return false;
        }
        Article other = (Article) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.lodz.p.it.spjava.purchasinginvoicessystem.model.Article[ id=" + id + " ]";
    }
    
    
}
