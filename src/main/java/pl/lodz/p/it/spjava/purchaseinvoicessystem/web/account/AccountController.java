/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.account;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.AccountEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Employee;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named("accountSession")
public class AccountController implements Serializable {

    @EJB
    AccountEndpoint accountEndpoint;

    private Employee editedAccount;

    public Employee getEditedAccount() {
        return editedAccount;
    }

    public void editAccount() {
        accountEndpoint.editAccount(editedAccount);
    }

    public void getAccountToEdition(Employee employee) {
        editedAccount = accountEndpoint.getAccountToEdition(employee);
    }

    public void removeAccount() {
        accountEndpoint.removeAccount(editedAccount);
    }

    public String registrateSession() {
        ContextUtils.invalidateSession();
        return "main";
    }

    public String getLogin() {
        return ContextUtils.getUserName();
    }
}
