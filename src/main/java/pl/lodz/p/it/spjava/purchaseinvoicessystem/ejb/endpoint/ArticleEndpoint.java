/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.ArticleDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.InvoiceDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.facade.ArticleFacade;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
@Stateful
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@Interceptors({pl.lodz.p.it.spjava.purchaseinvoicessystem.interceptor.LoggingInterceptor.class})
public class ArticleEndpoint extends AbstractEndpoint{

    @EJB
    private ArticleFacade articleFacade;

    public void addArticle(ArticleDTO articleDto, InvoiceDTO invoice) {

        Article article = new Article();
        article.setNameOfArticle(articleDto.getNameOfArticle());
        
        double price = articleDto.getPrice();
        price = roundNumber(price);
        article.setPrice(price);
        if(articleDto.getQuantityOfArticles() == 0){ // Jeśli użytkownik nie wpisałby ilości wtedy nie ma walidacji i tutaj jest ustawiona wartośc jeden dla ilosci artykułu
            article.setQuantityOfArticles(1);
        }
        else{
        article.setQuantityOfArticles(articleDto.getQuantityOfArticles());
        }
        
        
        
        double taxRate = articleDto.getTaxForArticle();
        taxRate = roundNumber(taxRate);
        article.setTaxRate(taxRate);
        article.setInvoiceNumber(invoice.getNumber());
        
        double grossPrice = (articleDto.getPrice()* articleDto.getQuantityOfArticles());
        grossPrice = roundNumber(grossPrice);
        article.setTotalGrossPrice(grossPrice);
        
        double netPrice = (article.getTotalGrossPrice()*(1 - (article.getTaxRate()/100)));
        netPrice = roundNumber(netPrice);
        article.setTotalNetPrice(netPrice);
        
        double taxPayment = (article.getTotalGrossPrice() - article.getTotalNetPrice());
        taxPayment = roundNumber(taxPayment);
        article.setTaxForArticle(taxPayment);
        
        articleFacade.create(article);
    }
    
    public void addArticle(ArticleDTO articleDto, Invoice invoice){
        InvoiceDTO invoiceDto = new InvoiceDTO();
        invoiceDto.setNumber(invoice.getNumber());
        addArticle(articleDto, invoiceDto);
    }
    
    public List<Article> getArticles(InvoiceDTO invoiceDto){
         List<Article> allArticles = articleFacade.findAll();
         List<Article> chosenArticles = new ArrayList<>();
         for(Article article: allArticles){
         if(article.getInvoiceNumber().equals(invoiceDto.getNumber())){
            chosenArticles.add(article);
         }
    }
         return chosenArticles;
}
    
    public List<Article> getArticles(Invoice invoice){
        InvoiceDTO invoiceDto = new InvoiceDTO();
        invoiceDto.setNumber(invoice.getNumber());
        List<Article> list = getArticles(invoiceDto);
        
        return list;
    }
}