/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.web.invoice;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.ArticleDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.InvoiceDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.dto.VendorDTO;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.ArticleEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.utils.ContextUtils;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.ejb.endpoint.InvoiceEndpoint;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.exception.ArticleException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.exception.BaseException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.exception.InvoiceException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Article;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
@SessionScoped
@Named(value = "createPageBean")
public class CreateInvoicePageBean implements Serializable {

    @Inject
    private InvoiceEndpoint invoiceEndpoint;

    @Inject
    private ArticleEndpoint articleEndpoint;

    private InvoiceDTO invoice;

    private ArticleDTO articleDto;

    private VendorDTO vendorDto;

    private static final Logger LOG = Logger.getLogger(CreateInvoicePageBean.class.getName());

    public String addArticle() {

        try {
            if (articleDto.getNameOfArticle() == null) {
                throw new ArticleException("Wpisz nazwę artykułu, zanim go dodasz!");
            }
            
            articleEndpoint.addArticle(articleDto, invoice);
            articleDto = new ArticleDTO();
        } catch (ArticleException ae) {
            LOG.log(Level.SEVERE, "Zgłoszenie w metodzie akcji wyjatku: ", ae);
            ContextUtils.emitInternationalizedMessage("createInvoice:nameOfArticle", ae.getMessage());
            return null;
        }
        return "createInvoiceArticleAdded";
    }

    public ArticleDTO getArticleDto() {
        return articleDto;
    }

    public void setArticleDto(ArticleDTO articleDto) {
        this.articleDto = articleDto;
    }

    public List<Article> getArticles() {
        return articleEndpoint.getArticles(invoice);
    }

    public VendorDTO getVendorDto() {
        return vendorDto;
    }

    public void setVendorDto(VendorDTO vendorDto) {
        this.vendorDto = vendorDto;
    }

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoiceDto(InvoiceDTO invoice) {
        this.invoice = invoice;
    }

    public CreateInvoicePageBean() {
        invoice = new InvoiceDTO();
        articleDto = new ArticleDTO();
        vendorDto = new VendorDTO();
    }

    public String createInvoice() throws BaseException, ParseException {

        List<Invoice> listInvoices = invoiceEndpoint.getInvoices();
        try {
            for (Invoice listInvoice : listInvoices) {
                if (listInvoice.getNumber().equals(invoice.getNumber())) {
                    throw new InvoiceException("Faktura z takim numerem juz istnieje!");
                }
            }
            if(articleEndpoint.getArticles(invoice).isEmpty()){
                throw new ArticleException("Przed utworzeniem faktury dodaj przynajmniej jeden artykuł!");
            }
            invoiceEndpoint.createNewInvoice(invoice, vendorDto);
            invoice = new InvoiceDTO();
            articleDto = new ArticleDTO();
            vendorDto = new VendorDTO();
        } catch (InvoiceException ie) {
            LOG.log(Level.SEVERE, "Zgłoszenie w metodzie akcji wyjatku: ", ie);
            ContextUtils.emitInternationalizedMessage("createInvoice:number", ie.getMessage());
            return null;
        }
          catch (ArticleException ae) {
            LOG.log(Level.SEVERE, "Zgłoszenie w metodzie akcji wyjatku: ", ae);
            ContextUtils.emitInternationalizedMessage("createInvoice:nameOfArticle", ae.getMessage());
            return null;
        }
        return "successInvoice";
    }

    public String returnToInvoicesList() {
        return "newInvoices";
    }
}
