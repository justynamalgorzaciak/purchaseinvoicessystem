/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.exception;

import javax.persistence.PersistenceException;
import pl.lodz.p.it.spjava.purchaseinvoicessystem.model.Invoice;

/**
 *
 * @author Justyna Małgorzaciak
 */
public class InvoiceException extends BaseException{
    
        public final static String INVOICE_UNIQUE_NUMBER_MESSAGE = "Faktura z takim numerem już istnieje!";

    public InvoiceException(String message) {
        super(message);
    }

    private InvoiceException(String message, Throwable cause) {
        super(message, cause);
    }

    private InvoiceException(Throwable cause) {
        super(cause);
    }

    private InvoiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }

    public static InvoiceException createForUniqueInvoiceNumber(Invoice invoice,
            PersistenceException pe) {
        InvoiceException ie = new InvoiceException(INVOICE_UNIQUE_NUMBER_MESSAGE, pe);
        ie.invoice = invoice;
        return ie;
    }
}
