/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.it.spjava.purchaseinvoicessystem.dto;

/**
 *
 * @author Justyna Małgorzaciak
 */
public class ArticleDTO {
    
    private Long numberOfArticle;
    
    private String nameOfArticle;
    
    private double price;
    
    private int quantityOfArticles = 1;
    
    private double taxForArticle;

    public Long getNumberOfArticle() {
        return numberOfArticle;
    }

    public void setNumberOfArticle(Long numberOfArticle) {
        this.numberOfArticle = numberOfArticle;
    }

    public String getNameOfArticle() {
        return nameOfArticle;
    }

    public void setNameOfArticle(String nameOfArticle) {
        this.nameOfArticle = nameOfArticle;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantityOfArticles() {
        return quantityOfArticles;
    }

    public void setQuantityOfArticles(int quantityOfArticles) {
        this.quantityOfArticles = quantityOfArticles;
    }

    public double getTaxForArticle() {
        return taxForArticle;
    }

    public void setTaxForArticle(double taxForArticle) {
        this.taxForArticle = taxForArticle;
    }
    
    
}
